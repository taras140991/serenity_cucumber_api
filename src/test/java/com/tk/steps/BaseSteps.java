package com.tk.steps;

import io.restassured.http.Header;
import io.restassured.specification.RequestSpecification;
import net.serenitybdd.rest.SerenityRest;

public class BaseSteps {

    protected RequestSpecification request;

    private static final String BASE_URI = "https://movie-database-imdb-alternative.p.rapidapi.com";
    private static final Header API_KEY =
            new Header("x-rapidapi-key", "a234b611e6msh7d743558b63f0cep19e8e2jsn1bdef729af2b");

    public BaseSteps() {
        request = SerenityRest
                .given()
                .baseUri(BASE_URI)
                .header(API_KEY);
    }
}
