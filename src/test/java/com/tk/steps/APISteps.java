package com.tk.steps;


import com.tk.Endpoint;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.cucumber.datatable.CucumberDataTableException;
import io.cucumber.datatable.DataTable;
import net.serenitybdd.rest.SerenityRest;
import org.apache.http.HttpStatus;

import java.util.Map;

import static org.hamcrest.Matchers.equalTo;

public class APISteps extends BaseSteps {

    @Given("request with valid parameters")
    public void given(DataTable data) {
        try {
            Map<String, String> params = data.asMap(String.class, String.class);
            params.forEach((k, v) -> request.param(k, v));
        } catch (CucumberDataTableException e) {}
    }

    @When("^call endpoint \"([^\"]*)\"$")
    public void when(Endpoint endpoint) {
        request.get(endpoint.getUrl());
    }

    @Then("should get response with data")
    public void then(DataTable data) {
        Map<String, String> expectedData = data.asMap(String.class, String.class);
        SerenityRest.restAssuredThat(response -> {
            response
                    .statusCode(HttpStatus.SC_OK);
            expectedData.forEach((k, v)-> response.body(k, equalTo(v)));
        });
    }
}
