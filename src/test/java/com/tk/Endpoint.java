package com.tk;

import lombok.Getter;

public enum Endpoint {

    BY_ID_OR_TITLE(""),
    BY_SEARCH("");

    private final String url;

    Endpoint(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }
}
