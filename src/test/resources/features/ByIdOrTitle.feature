Feature: Getting movie by id or title

  @Automated @HappyPath
  Scenario: A valid movie returned for valid movie id
    Given request with valid parameters
      |i              |tt4154796        |
    When call endpoint "BY_ID_OR_TITLE"
    Then should get response with data
      |Year           |2019             |
      |Title          |Avengers: Endgame|
      |Country        |USA              |

  @Automated
  Scenario: A negative result returned when call without parameters
    Given request with valid parameters
      |                 |                     |
    When call endpoint "BY_ID_OR_TITLE"
    Then should get response with data
      |Response         |False                |
      |Error            |Incorrect IMDb ID.   |

  @Automated
  Scenario: A negative result returned when call with incorrect movie id
    Given request with valid parameters
      |i                |ta415479644          |
    When call endpoint "BY_ID_OR_TITLE"
    Then should get response with data
      |Response         |False                |
      |Error            |Incorrect IMDb ID.   |