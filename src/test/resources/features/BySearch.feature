Feature: Getting movie by search

  @Automated @HappyPath
  Scenario: A valid result returned for searching movie
    Given request with valid parameters
      |s                |Avengers Endgame   |
    When call endpoint "BY_SEARCH"
    Then should get response with data
      |totalResults     |4                  |
      |Response         |True               |
      |Search[0].Title  |Avengers: Endgame  |


  @Automated
  Scenario: A negative result returned for searching movie
    Given request with valid parameters
      |s                |Avengerssss Endgame  |
    When call endpoint "BY_SEARCH"
    Then should get response with data
      |Response         |False                |
      |Error            |Movie not found!     |