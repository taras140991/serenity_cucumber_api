###### **TAF for testing REST API using serenity and cucumber.**

API for testing - https://rapidapi.com/rapidapi/api/movie-database-imdb-alternative

- Structure
  - src/test/java/com/tk - Test runners and supporting code
  - src/test/resources/features - Feature files
  
- How to run:
  - Prerequisites: maven3, java8 or greater
  - JUnit:
    - go to **src/test/java/com/tk/** and run class **TestRunner.java** (will run all testcases with @Automated tag by default)
    - you can modify the tags you want to run from @CucumberOptions inside the class
  - Maven:
    - run command from base project: **mvn clean verify** (will run all testcases with @automated tag by default)
    - if you want to run different tags: ** mvn clean verify -Dcucumber.options="--tags @HappyPath"**
    - html report is generated when running previous commands - open target/site/serenity/index.html after run
    
- How to add new tests:
  - Create new *.feature file in src/test/resources/features/ folder
  - First line of feature file should be "Feature: <Feature name or description>"
  - Add testing scenarios for feature
  - Scenario structure:
    - Scenario tags -> "@Automated @HappyPath"
    - Scenario name -> Scenario: 'Scenario name'
    - Given step    -> Given request with valid parameters
                        |'param name' |'param value'   |
    - When step     -> When call endpoint "'enpoint name'"
    - Then step     -> Then should get response with data
                        |'expected param'|'value of expected param'|